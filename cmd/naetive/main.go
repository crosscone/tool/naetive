package main

import (
	"errors"
	"io/ioutil"
	"log"
	"regexp"
	"strings"
)

// NaeString represents naetive string
type NaeString struct {
	Key    string
	Params []string
	Value  string
}

func main() {
	fileBytes, err := ioutil.ReadFile("../../examples/app.nae")
	if err != nil {
		panic(err)
	}

	fileLines := strings.Split(string(fileBytes), "\n")
	for _, line := range fileLines {
		naeString, err := parseString(line)
		if err != nil {
			continue
		}

		log.Printf("`%s` -> `%s` -> `%s`\n",
			naeString.Key,
			strings.Join(naeString.Params, ", "),
			naeString.Value,
		)
	}
}

var stringRegexp = regexp.MustCompile(`^([a-z\.\-]+)(?:\(([a-z\-\, ]+)\))? +\x60(.+)\x60$`)

func parseString(str string) (*NaeString, error) {
	matches := stringRegexp.FindStringSubmatch(str)
	if len(matches) == 0 {
		return nil, errors.New("invalid string")
	}

	key := matches[1]
	params := matches[2]
	value := matches[3]

	naeString := &NaeString{
		Key:    key,
		Params: parseParams(params),
		Value:  value,
	}

	return naeString, nil
}

func parseParams(str string) []string {
	params := strings.Split(str, ",")
	for i := 0; i < len(params); i++ {
		param := strings.TrimSpace(params[i])
		if len(param) > 0 {
			params[i] = param
		}
	}

	return params
}
