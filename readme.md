# Naetive

Naetive is next generation localization framework suitable for all programming languages.
It compiles translation files into first-class language constructs, such as classes, structures, interfaces, etc.

# Features

- reuse of localization strings
- string identifiers with namespaces
- file includes
- comments
- object types
- templating
- plural string translation
- code generation

# Types

- string
- number
- custom types

# Examples

```
# Translation string.
# Strings consist of identifier and followed by its contents, which enclosed in ` characters.
button.ok `Ok`

# String reuse
message.press-ok `Press button {button.ok}`

# Translation string that should not be translated
branding.company-name[const] `CrossCone`

# Templating
message.group-invite(name, group) `${name} invited you to {$group}`

# Templating with types
message.your-age(age: number) `You are ${age} years old`

# Interface specification.
# Interface names are written using UpperCamelCase.
User(name, age: number)

# Templating with interfaces
message.hello(user: User) `Hello, ${user.name}`

# Number selectors
tooltip.tab.close(count: number) `Close ${count} ${count one(tab) other(tabs)}`
```

# To Do

- CLI tool (for compiling .nae files to progremming language code)