package naetive

import (
	"fmt"
	"strings"
)

// Naetive represents naetive framework instance
type Naetive struct {
	storages map[string]*Storage
}

// Storage contains translation data and allows to translate strings for single language
type Storage struct {
	fallbackStorage *Storage
	strings         map[string]string
}

// Translate allows to translate single string using string key and parameters
func (s *Storage) Translate(key string, keyvals ...string) string {
	str, ok := s.strings[key]
	if !ok {
		if s.fallbackStorage != nil {
			return s.fallbackStorage.Translate(key, keyvals...)
		}

		return key
	}

	replacements := keyvalsToMap(keyvals...)
	for key, replacement := range replacements {
		str = strings.ReplaceAll(str, fmt.Sprintf("${%s}", key), replacement)
	}

	return str
}

func keyvalsToMap(keyvals ...string) map[string]string {
	res := map[string]string{}

	key := ""
	for _, kv := range keyvals {
		if len(key) == 0 {
			key = kv
		} else {
			res[key] = kv
			key = ""
		}
	}

	return res
}
